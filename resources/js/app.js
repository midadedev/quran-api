require('./bootstrap');

window.Vue = require('vue');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.use(VueRouter)

import Vue from 'vue'
import VueRouter from "vue-router";
// Vue.component('github', require('./components/Github.vue').default);

import routes from "./routes";

import store from './store';

const app = new Vue({
        el: '#app',
        store,
        router: new VueRouter(routes),
    }
).$mount('#app');


