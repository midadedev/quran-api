<?php

namespace App\Http\Controllers;

use App\Modification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userLoggedIn = Auth::id();
        request()->except(['_token','user_id']);
        request()->merge([
            'user_id' => $userLoggedIn
        ]);
        $data = request()->all();
//        dd($data);
//        dd($userLoggedIn);
        if ($userLoggedIn) {
            $modification = Modification::updateOrCreate(
                [
                    'user_id' => $data['user_id'],
                    'post_id' => $data['post_id']
                ]
                , $data
            );
            return $modification;
        }
        return 'unauthenticated process';
    }

    public function repoModifications($id)
    {
//        $modificaions = Modification::where('post_id', $id)
//            ->whereEnabled(1)
//            ->whereApproved(1)
//            ->user()
//
////            ->select([
////                'user.name'
////            ])
//            ->get();
//        dd($id);
        $usersModification = User::whereHas('modifications', function($q) use($id){
           $q->where('post_id', $id)
               ->whereEnabled(1)
               ->whereApproved(1);
        })
        ->get();

        return $usersModification;
    }

    public function getRepoModificationsUser($user_id, $post_id)
    {
        $modificaions = Modification::where('user_id', $user_id)
            ->where('post_id', $post_id)
            ->whereEnabled(1)
            ->whereApproved(1)
            ->with('user')
            ->first();

        return $modificaions;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modification  $modification
     * @return \Illuminate\Http\Response
     */
    public function show(Modification $modification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modification  $modification
     * @return \Illuminate\Http\Response
     */
    public function edit(Modification $modification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modification  $modification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modification $modification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modification  $modification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modification $modification)
    {
        //
    }
}
