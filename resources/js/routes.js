import Github from "./components/Github";
import ViewRepo from "./components/ViewRepo";
import SuggestionEdit from "./components/SuggestionEdit";
import viewSuggestion from "./components/viewSuggestion";
import PageNotFound from "./components/PageNotFound";

export default{
    mode: "history",
    routes: [
        { path: '/', component: Github, name: 'github' },
        { path: '/view-repo/:full_name/:repo_id', component: ViewRepo, name: 'view-repo', props: true },
        { path: '/suggestions/:id/:full_name', component: SuggestionEdit, name: 'suggestions-repo', props: true },
        { path: '/view-suggestion/:user_id/:post_id', component: viewSuggestion, name: 'view-suggestion', props: true },
        // ... other routes ...
        // and finally the default route, when none of the above matches:
        { path: "*", component: PageNotFound, name: 'not-found' }
    ]
}
