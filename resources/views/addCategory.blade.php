@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <form action="{{route('storeCategory')}}" method="post">
                @csrf
                <input type="text" name="name" id="name" value="{{ old('name') }}"><br>
                @if ($errors->has('name'))
                <span class="text-danger">{{ $errors->first('name') }}</span><br>
                @endif
                <textarea name="description" id="description">{{ old('description') }}</textarea>
                <br>
                @if ($errors->has('description'))
                <span class="text-danger">{{ $errors->first('description') }}</span><br>
                    @endif
                <input type="text" name="image" id="image" value="{{ old('image') }}"><br>
                @if ($errors->has('image'))
                <span class="text-danger">{{ $errors->first('image') }}</span><br>
                        @endif
                <button type="submit">add category</button>
            </form>




        </div>
    </div>
</div>
@endsection
