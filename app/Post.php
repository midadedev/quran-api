<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $primaryKey='id';

    protected $fillable = [
        'id',
        'name',
        'description',
        'image',
        'user_id',
        'category_id',
        'url',
        'type'
    ];

    protected $table = 'post';

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function preference()
    {
        return $this->hasMany(Preference::class, 'post_id');
    }

    public function modifications()
    {
        return $this->hasMany(Modification::class, 'post_id');
    }
}
