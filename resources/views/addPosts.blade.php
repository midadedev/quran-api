@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form action="{{route('storePost')}}" method="post">
                @csrf
                <select name="category_id" id="category">
                    @foreach($categories as $cate)
                        <option value="{{$cate->id}}">{{$cate->name}}</option>
                    @endforeach
                </select>
                <input type="text" name="name" id="name" value="{{ old('name') }}"><br>
                @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span><br>
                @endif
                <textarea name="description" id="description" >{{ old('description') }}</textarea>
                <br>
                @if ($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span><br>
                @endif
                <input type="text" name="image" id="image" value="{{ old('image') }}"><br>
                @if ($errors->has('image'))
                    <span class="text-danger">{{ $errors->first('image') }}</span><br>
                @endif
                <input type="text" name="url" id="image" value="{{ old('url') }}"><br>
                @if ($errors->has('url'))
                    <span class="text-danger">{{ $errors->first('url') }}</span><br>
                @endif
                <button type="submit">add post</button>
            </form>
        </div>
    </div>
</div>
@endsection
