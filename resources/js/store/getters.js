export default {
    repos: state => {
        return state.repos
    },
    body: state => {
        return state.body
    },
    selectedRepo: state => {
        return state.selectedRepo
    },
    usersRepo: state => {
        return state.usersRepo
    },
    usersRepoModification: state => {
        return state.usersRepoModification
    }
}
