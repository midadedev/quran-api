<?php

namespace App\Http\Controllers;

use App\Http\Requests\PreferenceRequest;
use App\Post;
use App\Preference;
use Illuminate\Http\Request;

class PreferenceController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function store(PreferenceRequest $request)
    {
        $data = request()->except(['_token']);
        $preference = Preference::updateOrCreate(
            [
                'user_id'=> $data['user_id'],
                'post_id'=>$data['post_id']
            ]
            ,$data
        );
        return $preference;
    }
}
