<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'image',
    ];
    protected $table = 'category';
    //

    public function posts()
    {
        $this->hasMany(Post::class, 'category_id');
    }


}
