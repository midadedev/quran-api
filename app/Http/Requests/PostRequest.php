<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|unique:post',
            'name' => 'required|unique:post|max:255',
            'category_id' => 'nullable|max:8|exists:category,id',
            'description' => 'required|max:2000',
            'body' => 'nullable|max:6000',
            'image' => 'nullable|max:250',
            'url' => 'nullable|max:250',
            'type' => 'in:1,2,3'
        ];
    }
}
