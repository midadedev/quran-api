<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modifications', function (Blueprint $table) {
            $table->id();
            $table->text('modify');
            $table->boolean('approved')->default(0);
            $table->boolean('enabled')->default(1);
            $table->unsignedBigInteger('repo_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->bigInteger('post_id')->unsigned();
            // foreign keys
            $table->foreign('post_id')
                ->references('id')
                ->on('post');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modifications');
    }
}
