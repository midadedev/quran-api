<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('addCategory');
    }

    public function store(CategoryRequest $request)
    {
//        dd(123);
        Category::create($request->all());
    }

    public function edit(Category $category)
    {

    }

    public function update(Category $category)
    {

    }

    public function destroy(Category $category)
    {

    }
}
