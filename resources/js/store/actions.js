import axios from 'axios';
import showdown from "showdown";

export default {
    getRepose({commit, state}, payload) {
        // console.log(payload.quran)
        return new Promise((res, rej) => {
            axios.get(`https://api.github.com/search/repositories?q=${payload.quran}`)
                .then(resp => {
                    // console.log(resp.data.items)
                    commit('SET_GITHUB_REPOSE', resp.data.items)
                    res()
                })
                .catch(() => rej())
        });
    },
    // set base url
    setBaseUrl({commit}, payload) {
        // commit('SET_BASE_URL', payload.baseUrl)
    },
    // set default language
    // setDefaultLanguage({commit}, payload) {
    //     // commit('SET_DEFAULT_LANGUAGE', payload.language)
    // },
    getMDfiles({commit, state, dispatch}, payload) {
        // console.log(payload.full_name)
        let repoFullName = payload.full_name
        dispatch('addGithubRepo', payload)
        return new Promise((res, rej) => {
            axios.get(`https://api.github.com/repos/${repoFullName}/contents/README.md`)
                .then(resp => {
                    // console.log(resp.data.content)
                    // window.atob(resp.data.content)
                    let converter = new showdown.Converter(),
                        text = window.atob(resp.data.content);
                    let fileContent = converter.makeHtml(text);
                    commit('SET_GITHUB_MARKDOWN_BODY', fileContent)
                    dispatch('fb_parse')
                    res()
                })
                .catch(() => rej())
        });
    },
    fb_parse({dispatch}){
        setTimeout(()=>{
            if (typeof FB === "undefined") {
                dispatch('fbInit')
            } else {
                window.FB.XFBML.parse();
            }
        },0)
    },
    fbInit() {
        window.fbAsyncInit = function() {
            FB.init({
                appId: "253932818905790",
                autoLogAppEvents: true,
                xfbml: true,
                version: "v6.0"
            });
            FB.AppEvents.logPageView();
        };
        (function(d, s, id) {
            var js,
                fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ar_AR/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        })(document, "script", "facebook-jssdk");
    },
    addSuggestion({commit, state, dispatch}, payload) {
        // console.log(payload)
        return new Promise((res, rej) => {
            axios.post('/saveSuggest',{
                // selected: payload,
                id: payload.id,
                name: payload.name,
                description: payload.description,
                body: state.body,
                // image: ,
                url: payload.html_url,
                // type: ,
                _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            })
                .then(resp => {
                    console.log(resp)
                    // window.atob(resp.data.content)
                    // dispatch('fb_parse')
                    // let converter = new showdown.Converter(),
                    //     text = window.atob(resp.data.content);
                    // let fileContent = converter.makeHtml(text);
                    // commit('SET_GITHUB_MARKDOWN_BODY', fileContent)
                    res()
                })
                .catch(() => rej())
        });
    },
    addGithubRepo({commit, state, dispatch}, payload) {
        // console.log(payload)
        return new Promise((res, rej) => {
            axios.post('/saveSuggest',{
                // selected: payload,
                id: payload.id,
                name: payload.name,
                description: payload.description,
                body: state.body,
                // image: ,
                url: payload.html_url,
                // type: ,
                _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            })
                .then(resp => {
                    // console.log(resp)
                    res()
                })
                .catch(() => rej())
        });
    },
    addPreference({commit, state, dispatch}, payload) {
        // console.log(payload)
        // let repoFullName = payload.fullName
        return new Promise((res, rej) => {
            axios.post('/addPreferences', {
                post_id: payload.id,
                preference: payload.type,
                user_id:1,
                follow:0,
                _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            }).then(resp => {
                    // console.log(resp)
                    res()
                })
                .catch(() => rej())
        });
    },
    selectedRepoAction({commit, state, dispatch}, payload) {
        // console.log(payload)
        // let repoFullName = payload.fullName
        return new Promise((res, rej) => {
            axios.get(`https://api.github.com/repos/${payload}`).then(resp => {
                    // console.log(resp)
                commit('SET_GITHUB_SELECTED_REPO', resp.data)
                dispatch('getMDfiles', resp.data)
                    res()
                })
                .catch(() => rej())
        });
    },
    addModificationRequest({commit, state, dispatch}, payload) {
        // console.log(payload)
        // let repoFullName = payload.fullName
        return new Promise((res, rej) => {
            axios.post(`/addModificationRequest`,payload)
                .then(resp => {
                    // console.log(resp)
                    window.location.replace(`/view-suggestion/${resp.data.user_id}/${resp.data.post_id}`);
                // commit('SET_GITHUB_SELECTED_REPO', resp.data)
                // dispatch('getMDfiles', resp.data)
                    res()
                })
                .catch(() => rej())
        });
    },
    getRepoModifications({commit, state, dispatch}, payload) {
        // console.log(payload)
        // let repoFullName = payload.fullName
        return new Promise((res, rej) => {
            axios.get(`/repoModifications/${payload}`)
                .then(resp => {
                    console.log(resp)
                commit('SET_GITHUB_USERS_REPO', resp.data)
                // dispatch('getMDfiles', resp.data)
                    res()
                })
                .catch(() => rej())
        });
    },
    getRepoModificationsUser({commit, state, dispatch}, payload) {
        // console.log(payload)
        // let repoFullName = payload.fullName
        return new Promise((res, rej) => {
            axios.get(`/getRepoModificationsUser/${payload.user_id}/${payload.post_id}`)
                .then(resp => {
                    // console.log(resp)
                commit('SET_GITHUB_MODIFI_REPO', resp.data)
                // dispatch('getMDfiles', resp.data)
                    dispatch('fb_parse')

                    res()
                })
                .catch(() => rej())
        });
    },

}

