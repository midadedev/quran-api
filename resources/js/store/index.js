import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';
import actions from './actions';
import mutations from './mutations';
import getters from './getters';
// import sharedAdminState from './shred_admin/state';
// import sharedAdminActions from './shred_admin/actions';
// import sharedAdminMutations from './shred_admin/mutations';
// import sharedAdminGetters from './shred_admin/getters';
// // import noteActions from '../../note/store/actions';

Vue.use(Vuex);

const githubModule = {
    state,
    actions,
    mutations,
    getters,
};

// const sharedAdminModule = {
//     state: sharedAdminState,
//     actions: sharedAdminActions,
//     mutations: sharedAdminMutations,
//     getters: sharedAdminGetters,
// };

const store = new Vuex.Store({
    modules: {
        githubModule,
        // sharedAdminModule
    }
});
export default store;
