<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modification extends Model
{
    protected $table='modifications';

    protected $fillable=[
        'modify',
        'approved',
        'enabled',
        'user_id',
        'post_id',
        'full_name',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
