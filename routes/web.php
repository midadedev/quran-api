<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route::group(['prefix' => 'admin'], function () {
//});

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/saveCategory', 'CategoryController@store')->name('storeCategory');
Route::post('/savePost', 'PostController@store')->name('storePost');
Route::post('/saveSuggest', 'PostController@suggest')->name('storeSuggest');
Route::post('/addPreferences', 'PreferenceController@store')->name('addPreferences');

Route::get('/category', 'CategoryController@index')->name('categoryFrm');
Route::get('/post', 'PostController@index')->name('postFrm');

Route::post('/addModificationRequest', 'ModificationController@store')->name('addModificationRequest');
Route::post('/addImagePost', 'PostController@storeImage')
    ->name('storeImage');

Route::get('repoModifications/{id}', 'ModificationController@repoModifications')->name('repoModifications');
Route::get('getRepoModificationsUser/{user_id}/{post_id}', 'ModificationController@getRepoModificationsUser')->name('getRepoModificationsUser');

Route::get('/{vue_capture?}', [
    'as' => 'gitrepo.frontend.vue',
    'uses' => 'PreferenceController@index',
])->where('vue_capture', '[\/\w\.-]*');
