
export default {
    //Base URL
    ['SET_BASE_URL'](state, payload) {
        state.base_url = payload;
    },
    ['SET_GITHUB_REPOSE'](state, payload) {
        state.repos = payload;
    },
    ['SET_GITHUB_MARKDOWN_BODY'](state, payload) {
        state.body = payload;
    },
    ['SET_GITHUB_SELECTED_REPO'](state, payload) {
        state.selectedRepo = payload;
    },
    ['SET_GITHUB_USERS_REPO'](state, payload) {
        state.usersRepo = payload;
    },
    ['SET_GITHUB_MODIFI_REPO'](state, payload) {
        state.usersRepoModification = payload;
    },
}
