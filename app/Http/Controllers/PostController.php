<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth')->except('index');
    }

    public function index()
    {
        $categories = Category::all();
        return view('addPosts', compact('categories'));
    }

    public function store(PostRequest $request)
    {
//        dd($this->generateId());
        $request->merge([
            'id' => $this->generateId(),
            'type' => 2
        ]);
//        return $request->all();
        Post::create($request->all());
    }

    function generateId() {
        $number = mt_rand(10000000000, 99999999999); // better than rand()

        // call the same function if the barcode exists already
        if ($this->postIdExist($number)) {
            return $this->generateId();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    function postIdExist($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Post::whereId($number)->exists();
    }


    public function suggest(Request $request)
    {
        $postExist = $this->postIdExist($request->id);
        if (!$postExist)
            Post::create($request->all());
//        dd($request->selected);

        return 'done';
    }

    public function storeImage(Request $request)
    {
        dd($request->all());
    }


    public function edit(Post $post)
    {

    }

    public function update(Post $post)
    {

    }

    public function destroy(Post $post)
    {

    }
}
